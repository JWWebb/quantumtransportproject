import numpy as np
import scipy

#defining sigma operator where i is the ket and j is the bra
def sigOp(i,j):
    return np.array(i)[np.newaxis].T*np.array(j)

w_down = 1
w_up = 1
U = 1

z = [1, 0, 0, 0]
d = [0, 1, 0, 0]
u = [0, 0, 1, 0]
ud = [0, 0, 0, 1]

cDown = sigOp(z,d) + sigOp(u,ud)
cUp = sigOp(z,u) - sigOp(d,ud)
cDownT = sigOp(d,z) + sigOp(ud,u)
cUpT = sigOp(u,z) - sigOp(ud, d)

class masterEq
    
    def __init__(self, model, cDown):
        

def dOp(x):
    if x == "down":
        return 1, 1
    elif x == "up":
        return 1, -1
    else:
        raise Exception("oopsy")

def aOp(x):
    if x == "down":
        return sigOp(z,d), sigOp(u,ud)
    elif x == "up":
        return sigOp(z,u), sigOp(d,ud)
    else:
        raise Exception("oops")

def cOp(x):
    return sum( [ i * j for i, j in zip( dOp(x), aOp(x) ) ] )

def w(x):
    if x == "down":
        return w_down
    elif x == "up":
        return w_up
    else:
        raise Exception("oopsie whoopsie")
def Hamiltonian(x):
    if x == "down":
        return w(x)*sigOp(d,d)
    elif x == "up":
        return w(x)*sigOp(u,u)
    elif x == "updown":
        return w("down")*sigOp(d,d)+w("up")*sigOp(u,u)
    else:
        return w("down")*sigOp(d,d)+w("up")*sigOp(u,u) + (w_down + w_up + U)*sigOp(ud,ud)

#if you want the full hamiltonian, do something wrong
